package servlet;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

@WebServlet("/api/parser")
public class ParserServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws IOException {

        response.getWriter().print("Hello from parser servlet!");
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws IOException {
        String input = Util.readStream(request.getInputStream());
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> map
                = objectMapper.readValue(input, new TypeReference<Map<String, String>>(){});
        Map<String, String> newMap = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            StringBuilder builder = new StringBuilder();
            builder.append(entry.getKey());
            String key = builder.reverse().toString();
            builder.setLength(0);
            builder.append(entry.getValue());
            String value = builder.reverse().toString();
            newMap.put(key, value);
        }
        String output = objectMapper.writeValueAsString(newMap);
        response.setContentType("application/json");
        response.getWriter().print(output);
    }
}
