package servlet;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import servlet.model.Order;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/api/orders")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws IOException {

        response.getWriter().print("Hello from order servlet!");
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws IOException {
        String input = Util.readStream(request.getInputStream());
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> map
                = objectMapper.readValue(input, new TypeReference<Map<String, String>>(){});
        Order order = new Order();
        if (map.containsKey("orderNumber")) {
            order.setOrderNumber(map.get("orderNumber"));
        }
        String output = objectMapper.writeValueAsString(order);
        response.setContentType("application/json");
        response.getWriter().print(output);
    }
}
