package servlet.model;

public class Order {
    private static Long idNumber = 1L;

    private Long id;
    private String orderNumber;

    public Order() {
        setId();
    }

    public Long getId() {
        return id;
    }

    private void setId() {
        this.id = idNumber;
        idNumber++;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }
}
